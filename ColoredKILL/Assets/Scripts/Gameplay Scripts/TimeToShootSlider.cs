﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TimeToShootSlider : MonoBehaviour
{
    [SerializeField]
    private Image _shootTime_slider;
    [SerializeField]
    private LevelController _lvlController;
    [SerializeField]
    private Character _character;
    [SerializeField]
    private Color _sliderColor;
    private bool _canSlide = true;
    private float _shootTime;
    private float _timer;
    private bool _newLevelComes;
    public static bool isFilled;
    void Start()
    {
        _shootTime = _lvlController._levels[_lvlController._currentLevel].timeBetweenShoots;
        _lvlController._onNewLevel += newLevelListener;
        _character._onShoot += canSlideListener;
        StartCoroutine(TimeCounter());
        StartCoroutine(checkShootSliderFilled());
        StartCoroutine(TimeShooterSlide());

    }
    void canSlideListener()
    {
        if (!_canSlide)
        {
            _shootTime_slider.color = Color.white;
            _timer = 0;
            _canSlide = true;
            isFilled = false;
        }
    }
    void newLevelListener()
    {
        _newLevelComes = true;
    }
    IEnumerator TimeShooterSlide()
    {
        while(true)
        {          
            if(!_newLevelComes)
                _shootTime_slider.fillAmount = Mathf.Lerp(0, 1, _timer);
            yield return null;
        }
    }
    IEnumerator checkShootSliderFilled()
    {
        while(true)
        {
            if (_canSlide)
            {
                if (Utility.almostEqual(_timer, 1, 0.01f) || _timer >= 1f)
                {
                    _shootTime_slider.color = new Color(_sliderColor.r, _sliderColor.g, _sliderColor.b);
                    _canSlide = false;
                    isFilled = true;
                }
            }
            yield return null;
        }
    }
    IEnumerator TimeCounter()

    {
        while (true)
        {
            if (_canSlide)
            {
                if (!_newLevelComes)
                    _timer += TimeManager.deltaTime * 1f / _shootTime;
                else
                {
                    yield return new WaitForSeconds(2);
                    _newLevelComes = false;
                    _shootTime = _lvlController._levels[_lvlController._currentLevel].timeBetweenShoots;
                }
            }
            yield return null;
        }
    }
}
