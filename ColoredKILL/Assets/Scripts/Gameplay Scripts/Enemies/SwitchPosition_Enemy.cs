﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchPosition_Enemy : Strong_Enemy
{

    private List<Strong_Enemy> _activeRedEnemies;
    private List<Strong_Enemy> _activeBlueEnemies;
    private const float _stayTime = 2f;
    private bool _inMiddle;

    private Vector3 _middlePos;
    private Color color;

    public override void Init(Vector3 dir , float Speed , Color color , float dead_bulletNumbers)
    {
        InitializeLists();
        _middlePos = (_characterPos.position + transform.position) / 2f;
      
        base.Init(dir, Speed, color , dead_bulletNumbers);
        
    }
    void InitializeLists()
    {
        _activeRedEnemies = EnemyGenerator._availableRedEnemies;
        _activeBlueEnemies = EnemyGenerator._availableBlueEnemies;
    }
    public override void Dispawn()
    {
        _healthbar.fillAmount = 1;
        GenericSwitcherEnemyPooler.instance.pushEnemy(this);
    }
    protected override IEnumerator Move(Vector3 dir, float Speed)
    {
        while (_moving)
        {
            if (!_onSwitch)
            {    
                if (Vector3.Distance(transform.position, _middlePos) <= 5f && !_inMiddle)
                {
                    Transform _strongest;
                    Strong_Enemy s;
                    if (color == Color.red)
                    {
                        if (_activeBlueEnemies.Count > 0)
                        {
                            if ((s = findBestActiveSwitcher(_activeBlueEnemies)) != null)
                            {
                                _onSwitch = true;
                                _strongest = s.transform;
                                StartCoroutine(Switch(transform, _strongest, 2f));
                                s.MustSwitch(true , transform , 2f);                              
                            }
                        }
                    }
                    else
                    {
                        if (_activeRedEnemies.Count > 0)
                        {
                            if ((s = findBestActiveSwitcher(_activeRedEnemies)) != null)
                            {
                                _onSwitch = true;
                                _strongest = s.transform;
                                 StartCoroutine(Switch(transform, _strongest, 2f));
                                s.MustSwitch(true , transform , 2f);
                            }
                        }
                    }
                    _inMiddle = true;
                }
                transform.Translate(_dir * Speed * TimeManager.deltaTime);
               
            }
            yield return null;
        }

    }

    Strong_Enemy findBestActiveSwitcher(List<Strong_Enemy> availableSwitchers)
    {
        List<Strong_Enemy> strongerSwitchers = availableSwitchers;
        int _minIndex = 0;
        for (int i = 0; i < availableSwitchers.Count; i++)
        {
            if (availableSwitchers[i].getRemainderLifes() <= _lifes)
                strongerSwitchers.RemoveAt(i);
        }
        if (strongerSwitchers.Count <= 0)
            return null;
        float _minDistance = Utility.getDistance(transform.position.x, transform.position.z,
          strongerSwitchers[0].transform.position.x,
          strongerSwitchers[0].transform.position.z);


        for (int i = 1; i < availableSwitchers.Count; i++)
        {
            if (_minDistance > Utility.getDistance(transform.position.x, transform.position.z,
            strongerSwitchers[i].transform.position.x,
            strongerSwitchers[i].transform.position.z))
            {
                _minDistance = Utility.getDistance(transform.position.x, transform.position.z,
                strongerSwitchers[i].transform.position.x,
                strongerSwitchers[i].transform.position.z);
                _minIndex = i;
            }

        }
        return strongerSwitchers[_minIndex];
       
    }

}
