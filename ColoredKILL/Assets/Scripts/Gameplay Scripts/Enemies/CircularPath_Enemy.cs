﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularPath_Enemy : IMovable
{
    private float radius;
    private float _startRadius;
    private float _timer;
    private Vector3 _center;
    private const float _angleSpeed = 8;
    public  void Init(Vector3 dir, float Speed, Color color , Vector3 center)
    {
        _center = center;
        _startRadius = Mathf.Sqrt(Mathf.Pow(dir.x, 2) + Mathf.Pow(dir.z, 2));
        base.Init(dir, Speed, color);     
    }
  
    protected override IEnumerator Move(Vector3 dir, float Speed)
    {
        while (_moving)
        {
            _timer += TimeManager.deltaTime * 1f/Speed;
             radius = Mathf.Lerp(_startRadius, 0, _timer);
            float x = Mathf.Cos(_timer* _angleSpeed) * radius + _center.x;
            float z = Mathf.Sin(_timer* _angleSpeed) * radius + _center.z;
            transform.position = new Vector3(x , -12 , z);
            yield return null;
        }
    }
    public override void Dispawn()
    {
        _moving = false;
        GenericCircularEnemyPooler.instance.pushEnemy(this);
    }


}
