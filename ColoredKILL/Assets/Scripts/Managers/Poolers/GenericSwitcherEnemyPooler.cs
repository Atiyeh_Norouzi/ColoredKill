﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericSwitcherEnemyPooler : MonoBehaviour
{


    #region poolObjectsStruct
    [System.Serializable]
    public struct switcherEnemyObjStruct
    {
        public int amount;
        public GameObject prefab;
        public Stack<SwitchPosition_Enemy> pooledObjs;
    }
    #endregion
    public switcherEnemyObjStruct enemyWeWantToPool;
    GameObject enemyParent;

    // Use this for initialization
    private static GenericSwitcherEnemyPooler _instance;
    public static GenericSwitcherEnemyPooler instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GenericSwitcherEnemyPooler>();

            return _instance;
        }
    }
    void Awake()
    {
        _instance = this;
        CreatePools();
    }
    public void CreatePools()
    {
        createBullets();
    }
    public void createBullets()
    {
        enemyWeWantToPool.pooledObjs = new Stack<SwitchPosition_Enemy>();
        enemyParent = new GameObject(enemyWeWantToPool.prefab.name + "_parent");
        enemyParent.transform.SetParent(instance.gameObject.transform);

        for (int j = 0; j < enemyWeWantToPool.amount; j++)
        {

            GameObject GO = Instantiate(enemyWeWantToPool.prefab);
            SwitchPosition_Enemy c = GO.GetComponent<SwitchPosition_Enemy>();
            GO.SetActive(false);
            GO.transform.SetParent(enemyParent.transform);
            enemyWeWantToPool.pooledObjs.Push(c);
        }

    }


    public void pushEnemy(SwitchPosition_Enemy enemy)
    {
        enemy.gameObject.SetActive(false);
        enemyWeWantToPool.pooledObjs.Push(enemy);
        enemy.gameObject.transform.SetParent(enemyParent.transform);
        enemy.gameObject.transform.localPosition = Vector3.zero;
    }


    public SwitchPosition_Enemy getEnemy()
    {
        SwitchPosition_Enemy p = enemyWeWantToPool.pooledObjs.Pop();
        p.gameObject.SetActive(true);
        p.gameObject.transform.SetParent(null);
        return p;
    }


}
