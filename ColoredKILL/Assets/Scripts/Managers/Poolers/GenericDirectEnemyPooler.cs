﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericDirectEnemyPooler : MonoBehaviour
{


    #region poolObjectsStruct
    [System.Serializable]
    public struct directEnemyObjStruct
    {
        public int amount;
        public GameObject prefab;
        public Stack<MoveDirect_Enemy> pooledObjs;
    }
    #endregion
    public directEnemyObjStruct enemyWeWantToPool;
    GameObject enemyParent;

    // Use this for initialization
    private static GenericDirectEnemyPooler _instance;
    public static GenericDirectEnemyPooler instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GenericDirectEnemyPooler>();

            return _instance;
        }
    }
    void Awake()
    {
        _instance = this;
        CreatePools();
    }
    public void CreatePools()
    {
        createBullets();
    }
    public void createBullets()
    {
        enemyWeWantToPool.pooledObjs = new Stack<MoveDirect_Enemy>();
        enemyParent = new GameObject(enemyWeWantToPool.prefab.name + "_parent");
        enemyParent.transform.SetParent(instance.gameObject.transform);

        for (int j = 0; j < enemyWeWantToPool.amount; j++)
        {

            GameObject GO = Instantiate(enemyWeWantToPool.prefab);
            MoveDirect_Enemy c = GO.GetComponent<MoveDirect_Enemy>();
            GO.SetActive(false);
            GO.transform.SetParent(enemyParent.transform);
            enemyWeWantToPool.pooledObjs.Push(c);
        }

    }


    public void pushEnemy(MoveDirect_Enemy enemy)
    {
        enemy.gameObject.SetActive(false);
        enemyWeWantToPool.pooledObjs.Push(enemy);
        enemy.gameObject.transform.SetParent(enemyParent.transform);
        enemy.gameObject.transform.localPosition = Vector3.zero;
    }


    public MoveDirect_Enemy getEnemy()
    {
        MoveDirect_Enemy p = enemyWeWantToPool.pooledObjs.Pop();
        p.gameObject.SetActive(true);
        p.gameObject.transform.SetParent(null);
        return p;
    }


}
