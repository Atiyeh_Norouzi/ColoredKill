﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericBulletPooler : MonoBehaviour {


    #region poolObjectsStruct
    [System.Serializable]
    public struct BulletObjStruct
    {
        public int amount;
        public GameObject prefab;
        public Stack<Bullet> pooledObjs;
    }
    #endregion
    public BulletObjStruct bulletsWeWantToPool;
    GameObject bulletParent;

    // Use this for initialization
    private static GenericBulletPooler _instance;
    public static GenericBulletPooler instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GenericBulletPooler>();

            return _instance;
        }
    }
    void Awake()
    {
        _instance = this;
        CreatePools();
    }
    public void CreatePools()
    {
        createBullets();
    }
    public void createBullets()
    {
        bulletsWeWantToPool.pooledObjs = new Stack<Bullet>();
        bulletParent = new GameObject(bulletsWeWantToPool.prefab.name + "_parent");
        bulletParent.transform.SetParent(instance.gameObject.transform);

        for (int j = 0; j < bulletsWeWantToPool.amount; j++)
        {

            GameObject GO = Instantiate(bulletsWeWantToPool.prefab);
            Bullet c = GO.GetComponent<Bullet>();
            GO.SetActive(false);
            GO.transform.SetParent(bulletParent.transform);
            bulletsWeWantToPool.pooledObjs.Push(c);
        }

    }
  

    public void pushBullet(Bullet bullet)
    {
        bullet.gameObject.SetActive(false);
        bulletsWeWantToPool.pooledObjs.Push(bullet);
        bullet.gameObject.transform.SetParent(bulletParent.transform);
        bullet.gameObject.transform.localPosition = Vector3.zero;
    }
  

    public Bullet GetBullet()
    {
        Bullet p = bulletsWeWantToPool.pooledObjs.Pop();
        p.gameObject.SetActive(true);
        p.gameObject.transform.SetParent(null);
        return p;
    }
   

}
