﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericColorEnemyPooler : MonoBehaviour
{


    #region poolObjectsStruct
    [System.Serializable]
    public struct colorEnemyObjStruct
    {
        public int amount;
        public GameObject prefab;
        public Stack<ColorChanger_Enemy> pooledObjs;
    }
    #endregion
    public colorEnemyObjStruct enemyWeWantToPool;
    GameObject enemyParent;

    // Use this for initialization
    private static GenericColorEnemyPooler _instance;
    public static GenericColorEnemyPooler instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GenericColorEnemyPooler>();

            return _instance;
        }
    }
    void Awake()
    {
        _instance = this;
        CreatePools();
    }
    public void CreatePools()
    {
        createBullets();
    }
    public void createBullets()
    {
        enemyWeWantToPool.pooledObjs = new Stack<ColorChanger_Enemy>();
        enemyParent = new GameObject(enemyWeWantToPool.prefab.name + "_parent");
        enemyParent.transform.SetParent(instance.gameObject.transform);

        for (int j = 0; j < enemyWeWantToPool.amount; j++)
        {

            GameObject GO = Instantiate(enemyWeWantToPool.prefab);
            ColorChanger_Enemy c = GO.GetComponent<ColorChanger_Enemy>();
            GO.SetActive(false);
            GO.transform.SetParent(enemyParent.transform);
            enemyWeWantToPool.pooledObjs.Push(c);
        }

    }


    public void pushEnemy(ColorChanger_Enemy enemy)
    {
        enemy.gameObject.SetActive(false);
        enemyWeWantToPool.pooledObjs.Push(enemy);
        enemy.gameObject.transform.SetParent(enemyParent.transform);
        enemy.gameObject.transform.localPosition = Vector3.zero;
    }


    public ColorChanger_Enemy getEnemy()
    {
        ColorChanger_Enemy p = enemyWeWantToPool.pooledObjs.Pop();
        p.gameObject.SetActive(true);
        p.gameObject.transform.SetParent(null);
        return p;
    }


}
