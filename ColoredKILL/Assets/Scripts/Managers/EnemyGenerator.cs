﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    [SerializeField]
    private LevelController _levelController;
    [SerializeField]
    private Transform[] _startPositions;
    [SerializeField]
    private Transform _characterPos;

    public static List<Strong_Enemy> _availableRedEnemies;
    public static List<Strong_Enemy> _availableBlueEnemies;
     
    private List<int> _usedLocations;
    private List<int> _availableLocations;

    private bool _canBuild;
    private bool _newlevelComes;

    private float _timeBetweenEnemies;
    void Start()
    {
        InitializeLists();
        InitializeAvaialablePositions(_levelController._levels[_levelController._currentLevel].maxLevelEnemies);
        _levelController._onNewLevel += newLevelListener;
        _canBuild = true;
        StartCoroutine(LerpTimeBetweenEnemies());
        StartCoroutine(BuildEnemies());
    }
    void InitializeLists()
    {
        _availableLocations = new List<int>();
        _usedLocations = new List<int>();
        _availableRedEnemies = new List<Strong_Enemy>();
        _availableBlueEnemies = new List<Strong_Enemy>();
    }
    void InitializeAvaialablePositions(int max)
    {
        for (int i = 0 ; i < max; i++)
        {
            int number;
            do number = Random.Range(0 , 8);
            while (_availableLocations.Contains(number));

            _availableLocations.Add(number);
        }
    }
    void newLevelListener()
    {
        _newlevelComes = true;
        DispawnAllEnemiesinTheScene();
    }
    IEnumerator BuildEnemies()
    {
        while (true)
        {
            //game over
            if (TimeManager.deltaTime == 0)
                _canBuild = false;
            if (_canBuild && !LevelController._newLevelComes)
            {            
                OnSpawnEnemies();
                resetLocations();
                yield return new WaitForSeconds(_timeBetweenEnemies);
            }
            yield return null;
        }
    }
    IEnumerator LerpTimeBetweenEnemies()

    {
        while (true)
        {
            if (!_newlevelComes)
                _timeBetweenEnemies = Mathf.Lerp(_levelController._levels[_levelController._currentLevel].maxTimeBetweenEnemies,
                   _levelController._levels[_levelController._currentLevel].minTimeBetweenEnemies, 1f/_levelController.getTimer() );
            yield return null;
        }
    }
    void OnSpawnEnemies()
    {
        switch (_levelController._levels[_levelController._currentLevel].type)
        {
            case LevelController.levelType.easy:
                {
                    SetStopEnemy();
                    SetDirectEnemy();
                    SetStrongEnemy();
                    break;
                }

            case LevelController.levelType.semieasy:
                {
                    SetColorEnemy();
                    SetDirectEnemy();
                    SetStrongEnemy();
                    break;
                }

            case LevelController.levelType.normal:
                {
                    SetGenerateEnemy();
                    SetDirectEnemy();
                    break;
                }
            case LevelController.levelType.seminormal:
                {
                    SetCircularEnemy();
                    SetDirectEnemy();
                    SetStrongEnemy();
                    break;
                }
            case LevelController.levelType.hard:
                {
                    SetColorEnemy();
                    SetCircularEnemy();
                    SetStrongEnemy();
                    break;
                }
            case LevelController.levelType.semihard:
                {
                    SetGenerateEnemy();
                    SetColorEnemy();
                    SetCircularEnemy();
                    break;
                }
            case LevelController.levelType.master:
                {
                    SetGenerateEnemy();
                    SetStrongEnemy();
                    break;
                }
            case LevelController.levelType.semimaster:
                {
                    SetSwitchEnemy();
                    SetColorEnemy();
                    SetStrongEnemy();                  
                    break;
                }
            case LevelController.levelType.heroic:
                {
                    SetCircularEnemy();
                    SetSwitchEnemy();
                    SetStrongEnemy();
                    break;
                }
        }

    }
    void resetLocations()
    {
        _availableLocations.Clear();
        InitializeAvaialablePositions(_levelController._levels[_levelController._currentLevel].maxLevelEnemies);
        _usedLocations.Clear();
    }
    void SetDirectEnemy()
    {
        int random;
        if (_availableLocations.Count <= 0)
            return;
        var directNumbers = getRandom(1, _availableLocations.Count);
        for (int i = 0; i < directNumbers; i++)
        {
            random = _availableLocations[getRandom(0, _availableLocations.Count)];
            if (i != 0)
            {
                while (_usedLocations.Contains(random))
                {
                    random = _availableLocations[getRandom(0, _availableLocations.Count)];
                }
            }
            MoveDirect_Enemy directGO = GenericDirectEnemyPooler.instance.getEnemy();
            directGO.transform.position = _startPositions[random].position;
            directGO.transform.rotation = _startPositions[random].rotation;
            directGO.Init((_characterPos.position - directGO.transform.position).normalized, 11f, getRandomColor());
            _usedLocations.Add(random);
            _availableLocations.Remove(random);
        }
    }
    void SetColorEnemy()
    {
        int random;
        if (_availableLocations.Count <= 0)
            return;
        var colorNumbers = getRandom(1, _availableLocations.Count);
        for (int i = 0; i < colorNumbers; i++)
        {
            random = _availableLocations[getRandom(0, _availableLocations.Count)];
            if (i != 0)
            {
                while (_usedLocations.Contains(random))
                {
                    random = _availableLocations[getRandom(0, _availableLocations.Count)];
                }
            }
            ColorChanger_Enemy colorGO = GenericColorEnemyPooler.instance.getEnemy();
            colorGO.transform.position = _startPositions[random].position;
            colorGO.transform.rotation = _startPositions[random].rotation;
            colorGO.Init((_characterPos.position - colorGO.transform.position).normalized, 8f, getRandomColor(), 3f);
            _usedLocations.Add(random);
            _availableLocations.Remove(random);
        }
    }
    void SetStopEnemy()
    {
        int random;
        if (_availableLocations.Count <= 0)
            return;
        var stopNumbers = Random.Range(1, _availableLocations.Count);        
        for (int i = 0; i < stopNumbers; i++)
        {
            random = _availableLocations[getRandom(0, _availableLocations.Count)];
            if (i != 0)
            {
                while (_usedLocations.Contains(random))
                {
                    random = _availableLocations[getRandom(0, _availableLocations.Count)];
                }
            }
            StopAndGo_Enemy stopGO = GenericStopEnemyPooler.instance.getEnemy();
            stopGO.transform.position = _startPositions[random].position;
            stopGO.transform.rotation = _startPositions[random].rotation;
            stopGO.Init((_characterPos.position - stopGO.transform.position).normalized, 14f, getRandomColor());
            _usedLocations.Add(random);
            _availableLocations.Remove(random);
        }
    }
    void SetGenerateEnemy()
    {
        int random;
        if (_availableLocations.Count <= 0)
            return;
        var generateNumbers = Random.Range(1, _availableLocations.Count);       
        for (int i = 0; i < generateNumbers; i++)
        {
            random = _availableLocations[getRandom(0, _availableLocations.Count)];
            if (i != 0)
            {
                while (_usedLocations.Contains(random))
                {
                    random = _availableLocations[getRandom(0, _availableLocations.Count)];
                }
            }
            GenerateChilds_Enemy birthGO = GenericBirthEnemyPooler.instance.getEnemy();
            birthGO.transform.position = _startPositions[random].position;
            birthGO.transform.rotation = _startPositions[random].rotation;
            Color color = getRandomColor();
            birthGO.Init((_characterPos.position - birthGO.transform.position).normalized, 8f, color,
                _levelController._levels[_levelController._currentLevel].bulletNumbersToKills);
            addToColorList(color, birthGO);
            _usedLocations.Add(random);
            _availableLocations.Remove(random);
        }
    }
    void SetSwitchEnemy()
    {
        int random;
        if (_availableLocations.Count <= 0)
            return;
        var switchNumbers = Random.Range(1, _availableLocations.Count);
        for (int i = 0; i < switchNumbers; i++)
        {
            random = _availableLocations[getRandom(0, _availableLocations.Count)];
            if (i != 0)
            {
                while (_usedLocations.Contains(random))
                {
                    random = _availableLocations[getRandom(0, _availableLocations.Count)];
                }
            }
            SwitchPosition_Enemy switchGO = GenericSwitcherEnemyPooler.instance.getEnemy();
            switchGO.transform.position = _startPositions[random].position;
            switchGO.transform.rotation = _startPositions[random].rotation;
            Color color = getRandomColor();
            switchGO.Init((_characterPos.position - switchGO.transform.position).normalized, 10f, color,
              _levelController._levels[_levelController._currentLevel].bulletNumbersToKills);
            addToColorList(color, switchGO);
            _usedLocations.Add(random);
            _availableLocations.Remove(random);
        }
    }
    void SetStrongEnemy()
    {
        int random;
        if (_availableLocations.Count <= 0)
            return;
        var strongNumbers = Random.Range(1, _availableLocations.Count);      
        for (int i = 0; i < strongNumbers; i++)
        {
            random = _availableLocations[getRandom(0, _availableLocations.Count)];
            if (i != 0)
            {
                while (_usedLocations.Contains(random))
                {
                    random = _availableLocations[getRandom(0, _availableLocations.Count)];
                }
            }
            Strong_Enemy strongGO = GenericStrongEnemyPooler.instance.getEnemy();
            strongGO.transform.position = _startPositions[random].position;
            strongGO.transform.rotation = _startPositions[random].rotation;
            Color color = getRandomColor();
            strongGO.Init((_characterPos.position - strongGO.transform.position).normalized, 10f, getRandomColor(),
             _levelController._levels[_levelController._currentLevel].bulletNumbersToKills);
            addToColorList(color, strongGO);
            _usedLocations.Add(random);
            _availableLocations.Remove(random);
        }
    }
    void SetCircularEnemy()
    {
        int random;
        if (_availableLocations.Count <= 0)
            return;
        var circularNumbers = Random.Range(0, 2);
        if (circularNumbers <= 0)
            return;     
        for (int i = 0; i < circularNumbers; i++)
        {
            random = _availableLocations[getRandom(0, _availableLocations.Count)];
            if (i != 0)
            {
                while (_usedLocations.Contains(random))
                {
                    random = _availableLocations[getRandom(0, _availableLocations.Count)];
                }
            }
            CircularPath_Enemy circularGO = GenericCircularEnemyPooler.instance.getEnemy();
            circularGO.transform.position = _startPositions[random].position;
            circularGO.transform.rotation = _startPositions[random].rotation;
            circularGO.Init((_characterPos.position - circularGO.transform.position), 20f, getRandomColor() , _characterPos.position);
            _usedLocations.Add(random);
            _availableLocations.Remove(random);
        }
    }
    Color getRandomColor()
    {
        int rand = getRandom(0, 2);
        if (rand == 0)
            return Color.red;
        return Color.blue;
    }
    int getRandom(int start, int finish)
    {
        return Random.Range(start, finish);
    }
    void DispawnAllEnemiesinTheScene()
    {
        GameObject[] _activeEnemies = GameObject.FindGameObjectsWithTag("enemy");
        for (int i = 0; i < _activeEnemies.Length; i++)
        {
            _activeEnemies[i].GetComponent<IMovable>().Dispawn();
        }

    }
    void addToColorList(Color color , Strong_Enemy s)
    {
        if (color == Color.red)
            _availableRedEnemies.Add(s);
        else
            _availableBlueEnemies.Add(s);
    }
}
