﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{
    public enum levelType { easy, semieasy, normal, seminormal, hard, semihard, master, semimaster, heroic }
    [System.Serializable]
    public struct LevelStruct
    {
        public levelType type;
        public float levelTime;
        public float timeBetweenShoots;
        public float shootSpeed;
        public float minTimeBetweenEnemies;
        public float maxTimeBetweenEnemies;
        public float bulletNumbersToKills;
        public int maxLevelEnemies;
    }
    public delegate void Deleg_newLevel();
    public event Deleg_newLevel _onNewLevel;
    [SerializeField]
    public LevelStruct[] _levels;
    [SerializeField]
    private Image heart_slider;
    [SerializeField]
    private Animator _arrowAnimator;
    public int _currentLevel;
    private float _timeCounter;
    public static bool _newLevelComes;
    private static float _timeToshoot;
    private static float _bulletPower;
    private  const string _animatorName =  "arrowUp_animationClip";
	void Start ()
    {
        setBulletProperties();
        StartCoroutine(ChangeLevel());
        StartCoroutine(TimeCounter());
	}
    IEnumerator ChangeLevel()
    {
        while(true)
        {
            if(Utility.almostEqual(_timeCounter , 1 , 0.01f))
            {
                if (_onNewLevel != null)
                    _onNewLevel();
                SetNewLevelState(true);
                yield return new WaitForSeconds(2f);
                SetNewLevelState(false);
                if (_currentLevel+1 != _levels[_currentLevel].levelTime)
                    _currentLevel++;
                setBulletProperties();
            }

            yield return null;
        }
    }
    void SetNewLevelState(bool start)
    {
        _arrowAnimator.gameObject.SetActive(start);
        if (start)
        {
            _arrowAnimator.PlayInFixedTime(_animatorName,-1, 0);
            heart_slider.fillAmount = 1;
        }
        _newLevelComes = start;
        _timeCounter = 0;
    }
    void setBulletProperties()
    {
        _timeToshoot = _levels[_currentLevel].timeBetweenShoots;
        _bulletPower = _levels[_currentLevel].shootSpeed;
    }
    IEnumerator TimeCounter()

    {
        while(true)
        {
            if(!_newLevelComes)
                _timeCounter += TimeManager.deltaTime * 1f /_levels[_currentLevel].levelTime;
            yield return null;
        }
    }

    public static float getTimeBetweenShoots()
    {
        return _timeToshoot;
    }
    public static float GetCurrentBulletPower()
    {
        return _bulletPower;
    }
    public float getTimer()
    {
        return _timeCounter;
    }
 
    void OnLevelWasLoaded()
    {
        _onNewLevel = null;
    }
}
