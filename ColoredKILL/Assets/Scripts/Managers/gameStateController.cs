﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class gameStateController : MonoBehaviour
{
    [SerializeField]
    private GameObject panel;
    [SerializeField]
    private Button retryBtn;
    [SerializeField]
    private Button quitBtn;
    [SerializeField]
    private Text txt_timer;
    [SerializeField]
    private Text _score;
    private float _timer;
    private static gameStateController _instance;
    public static gameStateController instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<gameStateController>();
            return _instance;
        }
    }

    // Use this for initialization
    void Start()
    {
        retryBtn.onClick.AddListener(delegate () { Retry(); });
        quitBtn.onClick.AddListener(delegate () { Quit(); });
        StartCoroutine(timeCounter());
    }
    public void ActiveRetryPanel()
    {
        setScore();
        panel.SetActive(true);
    }
    void Retry()
    {
        TimeManager.timeScale = 1;
        SceneManager.LoadScene("Colored");

    }
    IEnumerator timeCounter()
    {
        while (true)
        {
            if (!LevelController._newLevelComes)
            {
                _timer += TimeManager.deltaTime;
                txt_timer.text = _timer.ToString("0");    
            }
            yield return null;
        }

    }
    void Quit()
    {
        Application.Quit();
    }
    void setScore()
    {
        _score.text = txt_timer.text;
    }
}
